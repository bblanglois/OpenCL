###### This work is licensed under a Creative Commons [Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)

#  OpenCL Host Programming

The aim of this coursework is to create a simple host-side driver routine run driver() to interact with an OpenCL-compliant device (e.g. a GPU). This run driver() routine is called from a multithreaded testbench which will take care of initialisation and shutdown of the device, creation of the data to be sent to the device and validation of the returned result.  A testbench is provided to test the programm efficency and and the correct use of the multi-threaded library Pthreads in C.

By Benjamin Langlois 2018